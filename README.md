# Cashier

Spells out the price you enter.

## Prerequisites

Tested on MacOS with Rust v1.29.0.

## Build the project

```
cargo build
```

## Run the command

```
./target/debug/cashier
```

Example:


\> Enter a price

\> 15478

\> You have to pay ~~One Thousand, Five Hundred, Four Hundred Seventy Eight~~ Fifteen Thousand, Four Hundred Seventy Eight


## Run tests

```
cargo test
```
