use std::collections::{LinkedList};
use std::io;

static NAMES: [(&str, usize, bool); 33] = [
    ("Trillion", 1000000000000, true),
    ("Billion", 1000000000, true),
    ("Million", 1000000, true),
    ("Thousand", 1000, true),
    ("Hundred", 100, false),
    ("Ninety", 90, false),
    ("Eighty", 80, false),
    ("Seventy", 70, false),
    ("Sixty", 60, false),
    ("Fifty", 50, false),
    ("Fourty", 40, false),
    ("Thirty", 30, false),
    ("Twenty", 20, false),
    ("Nineteen", 19, false),
    ("Eighteen", 18, false),
    ("Seventeen", 17, false),
    ("Sixteen", 16, false),
    ("Fifteen", 15, false),
    ("Fourteen", 14, false),
    ("Thirteen", 13, false),
    ("Twelve", 12, false),
    ("Eleven", 11, false),
    ("Ten", 10, false),
    ("Nine", 9, false),
    ("Eight", 8, false),
    ("Seven", 7, false),
    ("Six", 6, false),
    ("Five", 5, false),
    ("Four", 4, false),
    ("Three", 3, false),
    ("Two", 2, false),
    ("One", 1, false),
    ("Zero", 0, false),
];

fn main() {
    let mut number_string = String::new();
   
    println!("Enter a price");
    let read = io::stdin().read_line(&mut number_string);
    match read {
        Ok(num_bytes) => {
            number_string.truncate(num_bytes - 1); // remove the termination character
            parse_number(&mut number_string)
        },
        Err(err) => println!("Failed to read the input: {}", err),
    }
}

fn parse_number(number_string: &mut String) {
    println!("You entered {}", number_string);

    let maybe_number = number_string.parse::<usize>();
    match maybe_number {
        Ok(number) => println!("You have to pay {}", get_spelling(number)),
        Err(err) => println!("What you entered is not a positive number: {}", err),
    }
}

#[derive(Debug)]
enum Step {
    Num(usize, usize),
    Str(String),
}

fn get_spelling(number: usize) -> String {
    let mut spelling = String::new();

    // A recursive implementation is concise and beautiful but unfortunately
    // I can't use recursion here due to the stack limit.
    let mut stack: LinkedList<Step> = LinkedList::new();

    stack.push_front(Step::Num(number, 0));

    loop {
        let maybe_element = stack.pop_front();

        match maybe_element {
            Some(Step::Str(string)) => {
                if spelling.len() != 0 {
                    // Separate all the words by space.
                    spelling.push_str(" ");
                }
                spelling.push_str(&string);
            },
            Some(Step::Num(num, power)) => {
                let (name, threshold, needs_comma) = NAMES[power];

                if num == threshold {
                    stack.push_front(Step::Str(name.to_string()));
                } else {
                    let past_threshold = num / threshold;

                    if past_threshold == 0 {
                        // Continue searching for a single word representing the biggest number
                        // that fits into the given number.
                        stack.push_front(Step::Num(num, power + 1));
                    } else {
                        // Split the number into whatever comes after the given word, the word
                        // itself, and its multiplier, like "Hundred Two", "Billion", and "Hundred Million".
                        if num % threshold != 0 {
                            stack.push_front(Step::Num(num % threshold, power + 1));
                        }

                        let delimiter = if needs_comma {",".to_string()} else {"".to_string()};
                        stack.push_front(Step::Str(name.to_string() + &delimiter));

                        if past_threshold > 1 {
                            stack.push_front(Step::Num(past_threshold, power));
                        }
                    }
                }
            },
            None => break spelling,
        }
    }
}

#[cfg(test)]
mod get_spelling_tests {
    use get_spelling;

    #[test]
    fn spells_one_word_numbers() {
        assert_eq!("Zero", &get_spelling(0));
        assert_eq!("One", &get_spelling(1));
        assert_eq!("Eleven", &get_spelling(11));
        assert_eq!("Twenty", &get_spelling(20));
        assert_eq!("Ninety", &get_spelling(90));
        assert_eq!("Hundred", &get_spelling(100));
        assert_eq!("Thousand", &get_spelling(1000));
    }

    #[test]
    fn combines_words() {
        assert_eq!("Twenty One", &get_spelling(21));
        assert_eq!("Thirty Three", &get_spelling(33));
        assert_eq!("Eighty Five", &get_spelling(85));
        assert_eq!("Hundred Twenty Six", &get_spelling(126));
        assert_eq!("Two Hundred Seventy", &get_spelling(270));
    }

    #[test]
    fn puts_comma_where_appropriate() {
        assert_eq!("Two Billion, Three Hundred Million, Two Hundred Thousand, Eight Hundred Twenty Four", &get_spelling(2300200824));
    }
}
